#!/usr/bin/env bash
set -x

GITLAB_SERVER="gitlab.com"

# Get role name/path for formatted mattermost message
role_path=$(curl -k -H "PRIVATE-TOKEN: $GITLAB_CI_TOKEN" "https://$GITLAB_SERVER/api/v4/projects/$CI_PROJECT_ID" | tr \, \\12|grep path_with_namespace | cut -d: -f2|tr -d \")

# return boolean value to identify if the last commit was related to an approved merge request
merge=$(curl -k -H "PRIVATE-TOKEN: $GITLAB_CI_TOKEN" "https://$GITLAB_SERVER/api/v4/projects/$CI_PROJECT_ID/repository/commits" | python -c "import sys, json, re; last_commit=json.load(sys.stdin)[0]['message']; print(bool(re.search(\"Merge branch\", last_commit)))")

# return boolean value to identify if a major bump is required - searches for "MAJOR-BUMP" in the commit message
major_bump=$(curl -k -H "PRIVATE-TOKEN: $GITLAB_CI_TOKEN" "https://$GITLAB_SERVER/api/v4/projects/$CI_PROJECT_ID/repository/commits" | python -c "import sys, json, re; last_commit=json.load(sys.stdin)[0]['message']; print(bool(re.search(\"MAJOR-BUMP\", last_commit)))")

echo "major_bump=" $major_bump
echo "merge=" $merge
echo "project_id=" $CI_PROJECT_ID

if [ "$merge" == "True" ] ; then

  current_major="$(git tag | grep ^[0-9]*\\.[0-9]*\\.[0-9]*$ |sort -Vr|head -1|awk -F. '{ print $1 }')"
  echo "current_major:" $current_major
  current_minor="$(git tag | grep ^[0-9]*\\.[0-9]*\\.[0-9]*$ |sort -Vr|head -1|awk -F. '{ print $2 }')"
  echo "current_minor:" $current_minor
  current_patch="$(git tag | grep ^[0-9]*\\.[0-9]*\\.[0-9]*$ |sort -Vr|head -1|awk -F. '{ print $3 }')"
  echo "current_patch:" $current_patch

TAG=$current_major"."$current_minor"."$current_patch

  # If first tag set to initial version
  if [ -z "$current_major" ] ; then
    new_major=1
    new_minor=0
    new_patch=0
  else
    new_major=$current_major
    new_minor=$current_minor
    new_patch=$((current_patch+1))
  fi

  if [ "$major_bump" == "True" ] ; then
    new_major=$((current_major+1))
    new_minor=0
    new_patch=0
  fi

TAG=$new_major"."$new_minor"."$new_patch

#Create the tag using the API

  curl -X POST -k -H "PRIVATE-TOKEN: $GITLAB_CI_TOKEN" "https://$GITLAB_SERVER/api/v4/projects/$CI_PROJECT_ID/repository/tags?tag_name=$TAG&ref=develop"


#Delete and re-Create "latest" tag pointing to this version
  curl -X DELETE -k -H "PRIVATE-TOKEN: $GITLAB_CI_TOKEN" "https://$GITLAB_SERVER/api/v4/projects/$CI_PROJECT_ID/repository/tags/latest"
  curl -X POST -k -H "PRIVATE-TOKEN: $GITLAB_CI_TOKEN" "https://$GITLAB_SERVER/api/v4/projects/$CI_PROJECT_ID/repository/tags?tag_name=latest&ref=$TAG"

fi
